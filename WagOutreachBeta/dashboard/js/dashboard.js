﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

var status_of_corporate_clinics;
var outreach_activity;
var scheduled_completed_clinics;
var fiscal_year_senior_outreach;
var so_businesses_events;
var local_opportunities;
var top_performing_stores;

$('#_dashboard').live('pagebeforecreate', function (event) {
    //
    status_of_corporate_clinics = google.visualization.arrayToDataTable([
              ['Task', 'Hours per Day'],
              ['Clinics to Review/Assign', 82],
              ['Clinics to Confirm', 531],
              ['Clinics to Complete', 505],
              ['Clinics Completed', 700],
              ['Clinics Canceled', 300]
    ]);
    //
    outreach_activity = new google.visualization.DataTable();
    outreach_activity.addColumn('string', 'Month');
    outreach_activity.addColumn('number', 'Immunization');
    outreach_activity.addColumn('number', 'Senior Outreach');

    outreach_activity.addRows([     
       ['Nov', 1545, 1612 ],
       ['Dec', 4816, 4373],
       ['Jan', 2667, 6988],
       ['Feb', 1107, 7137 ],
       ['Mar', 1841, 7036],
       ['Apr', 1450, 8520 ],
       ['May', 4282, 7324],
       ['Jun', 65515, 10136],
       ['Jul', 101545, 7798],
       ['Aug', 92807, 7589],
       ['Sep', 64751, 7151],
       ['Oct', 26207, 4482]
    ]);
    //
    scheduled_completed_clinics = google.visualization.arrayToDataTable([
             ['Season', 'Local', 'Charity', 'Corporate', 'Vote & Vax'],
                ['IMZ 2016-17', 26467, 6735, 2336, 36],
                ['IMZ 2015-16', 22315, 5435, 2526, 0],
                ['IMZ 2014-15', 15797, 3268, 1740, 0]
    ]);
    //
    fiscal_year_senior_outreach = google.visualization.arrayToDataTable([
            ['Element', 'Scheduled Events'],
            ['Social Event', 318],
            ['Health & Wellness', 1378],
            ['Presentation', 412],
            ['Education', 161],
            ['Drop Off Materials', 933],
            ['Local Business', 35],
            ['Open House', 37],
            ['Flu/Immunization', 285],
            ['Other', 329],
    ]);
    //
    so_businesses_events = google.visualization.arrayToDataTable([              
              ['Events', 'Events Scheduled', 'Businesses'],
              ['SO 2017', 3687,	3053],
              ['SO 2016', 27851, 13836],
              ['SO 2015', 29271, 14022]
    ]);
    //
    local_opportunities = google.visualization.arrayToDataTable([
              ['Task', 'Hours per Day'],
              ['HCS Local Leads', 8211],
              ['HCS Direct Mail B2B Leads', 3977],
              ['DM Local Leads', 2505],
              ['DM Direct Mail B2B Leads', 1208],

    ]);
    //
     top_performing_stores = google.visualization.arrayToDataTable([
        ['ID', '% of Assigned Opportunities to Contracts', 'Clinics Completed', 'Region', 'Immunizations Administered'],
        ['4551',	22,  45,   'Region 1',   1272], 
        ['7550',	59,  43,   'Region 21',  1037], 
        ['3652',	28,  38,   'Region 20',  1045], 
        ['4294',	50,  35,   'Region 13',  1327], 
        ['6221',	36,  35,   'Region 6',   1681], 
        ['2501',	13,  33,   'Region 20',  661], 
        ['12300',	15,  32,   'Region 19',  947], 
        ['9854',	42,  32,   'Region 27',  1440], 
        ['13086',	2,   32,   'Region 1',   1279], 
        ['6889',	57,  32,   'Region 13',  1103], 
        ['15260',	6,   30,   'Region 28',  577], 
        ['3502',	47,  30,   'Region 7',   725], 
        ['3063',	64,  29,   'Region 6',   813], 
        ['5852',	19,  29,   'Region 21',  494], 
        ['6485',	22,  28,   'Region 13',  500], 
        ['4775',	38,  27,   'Region 13',  471], 
        ['7177',	32,  26,   'Region 28',  981], 
        ['11675',	33,  25,   'Region 15',  781], 
        ['4045',	39,  24,   'Region 5',   600]
    ]);
});

$(document).on('pageshow', '#_dashboard', function (event) {
    if ($(window).width() <= 414) {
        $('#tdRightHeader').css('display', 'none');
    }
    else {
        $('#tdRightHeader').css('display', 'block');
        $('#tdRightHeader').css('display', '');
    }

    loadCharts();
});

function loadCharts() {
    displayCorporateClinicsStatus();
    displayOutreachActivity();
    displayScheduledCompletedClinics();
    displayFiscalYearSeniorOutreach();
    displaySOBusinessesEvents();
    displayLocalOpportunities();
    displayTopPerformingStores();
}

function displayLocalOpportunities() {
    var options = {
        title: 'Local Opportunities to Review/Assign',
        animation: {
            "startup": true,
            duration: 1000,
            easing: 'out'
        },
        //is3D: true,
    };
    drawGoogleChart(local_opportunities, options, "pie", "piechart_local_opportunities");
}

function displaySOBusinessesEvents() {
    var options = {
        title: 'Senior Outreach Businesses & Events Year Over Year',
        chartArea: { width: '50%' },
        colors: ['#3366cc', '#7ac143'],
        animation: {
            "startup": true,
            duration: 1000,
            easing: 'out'
        },
        hAxis: {
            title: 'Events',
            minValue: 0
        },
        vAxis: {
            title: 'Fiscal Year'
        }
    };
    drawGoogleChart(so_businesses_events, options, "bar", "barchart_so_businesses_events");
}

function displayFiscalYearSeniorOutreach() {
    var options = {
        title: 'Fiscal 2017 YTD Senior Outreach',
        colors: ['#7ac143'],
        animation: {
            "startup": true,
            duration: 1000,
            easing: 'out'
        },
        chartArea: { width: '50%', height: '80%' },
        bar: { groupWidth: "30%" }
    };

    drawGoogleChart(fiscal_year_senior_outreach, options, "bar", "barchart_fiscal_year_senior_outreach");
}

function displayScheduledCompletedClinics() {
    var options = {
        title: 'Scheduled & Completed Clinics Year Over Year',
        colors: ['red', '#7ac143', '#2b7bb7', 'grey'],
        chartArea: { width: '50%' },
        animation: {
            "startup": true,
            duration: 1000,
            easing: 'out'
        },
        hAxis: {
            title: 'Clinics',
            minValue: 0
        },
        vAxis: {
            title: 'Immunization Season'
        }
    };
    drawGoogleChart(scheduled_completed_clinics, options, "bar", "barchart_scheduled_completed_clinics");
}

function displayCorporateClinicsStatus() {
    var options = {
        title: 'Status of Corporate Clinics',
        animation: {
            "startup": true,
            duration: 1000,
            easing: 'out'
        },


        //is3D: true
    };
    drawGoogleChart(status_of_corporate_clinics, options, "pie", "piechart_status_of_corporate_clinics");
}

function displayOutreachActivity() {
    var options = {
        title: 'Outreach Activity',
        colors: ['#3366cc', '#7ac143'],
        //chartArea: { left: 35, width: '70%' },
        animation: {
            "startup": true,
            duration: 1000,
            easing: 'out'
        },
        hAxis: {
            title: 'Last 12 Months'
        },
        vAxis: {
            title: 'Logged Contacts'
        }
    };
    drawGoogleChart(outreach_activity, options, "column", "columnchart_outreach_activity");
}

function displayTopPerformingStores() {
    var options = {
        title: 'Top Performing Stores',
        hAxis: { title: '% of Assigned Opportunities to Contracts' },
        vAxis: { title: 'Clinics Completed' },
        chartArea:{width:'70%', height: '70%'},
        animation: {
            "startup": true,
            duration: 1000,
            easing: 'out'
        },
        bubble: { textStyle: { fontSize: 10 } }
    };
  
    drawGoogleChart(top_performing_stores, options, "bubble", "bubblechart_top_performing_stores");
}

function drawGoogleChart(data, options, chart_type, chart_div) {
    var google_chart;
    if (chart_type == "pie")
        google_chart = new google.visualization.PieChart(document.getElementById(chart_div));
    else if (chart_type == "bar")
        google_chart = new google.visualization.BarChart(document.getElementById(chart_div));
    else if (chart_type == "column")
        google_chart = new google.visualization.ColumnChart(document.getElementById(chart_div));
    else if (chart_type == "bubble")
        google_chart = new google.visualization.BubbleChart(document.getElementById(chart_div));

    google_chart.draw(data, options);
}