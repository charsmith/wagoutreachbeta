﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

$('#_clinicDetails').live('pagebeforecreate', function (event) {
   
});

$(document).on('pageshow', '#_clinicDetails', function (event) {
    if ($(window).width() <= 414) {
        $('#tdRightHeader').css('display', 'none');
    }
    else {
        $('#tdRightHeader').css('display', 'block');
        $('#tdRightHeader').css('display', '');
    }

    jQuery('#txtContactDate').datepicker({
        minDate: new Date(2010, 0, 1),
        dateFormat: 'mm/dd/yy',
        constrainInput: true,
        //beforeShowDay: function (date) {
        //    return displayBlockDays(date, 0);
        //}
    });

    $('.thermometer-noconfig').thermometer({
        speed: 'slow'
    });
    createPlaceHolderforIE();

});