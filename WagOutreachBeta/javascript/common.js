﻿jQuery.support.cors = true;
var url_string = unescape(window.location);
var query_string = url_string.substr(url_string.indexOf("?") + 1, url_string.length);

/* create an array of days which need to be disabled */
var disabledDays = ["0"];

function displayBlockDays(date, days) {
    var cur_date = new Date();
    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
    var d_day = date.getDay();

    var tmp_cur_date = cur_date;
    tmp_cur_date.setDate(cur_date.getDate() + days - 1);

    if (date < tmp_cur_date)
        return [false];
    else {
        for (i = 0; i < disabledDays.length; i++) {
            if (disabledDays[i] == d_day)
                return [false];
        }
        return [true, ''];
    }
}

function gotoHomePage() {
    window.setTimeout(function () {
        window.location.href = '../outreach.htm';
    }, 300);
};

function createPlaceHolderforIE() {
    if ($.browser.msie) {
        $('input[placeholder]').each(function () {
            var input = $(this);
            if ($(input).val() == "") $(input).val(input.attr('placeholder'));
            if ($(input).val() == '' || $(input).val().toLowerCase() == $(input).attr('placeholder').toLowerCase())
                $(input).css('color', 'grey');
            else
                $(input).css('color', 'black');

            $(input).focus(function () {
                if ($(input).val().toLowerCase() == $(input).attr('placeholder').toLowerCase()) {
                    input.val('').css('color', 'black');
                }
                else
                    $(input).css('color', 'black');
            });
            $(input).blur(function () {
                if ($(input).val() == '' || $(input).val().toLowerCase() == $(input).attr('placeholder').toLowerCase()) {
                    $(input).val($(input).attr('placeholder')).css('color', 'grey');
                }
                else
                    $(input).css('color', 'black');
            });
        });
    };
};