﻿/// <reference path="../../ClinicDetails/clinicDetails.htm" />
$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
var mapping = { 'ignore': ["__ko_mapping__"] }

var opportunitiesServiceUrl = '../JSON/_opportunities.JSON';
var scheduledEventsUrl = '../JSON/_scheduledEvents.JSON';
var storeTasksUrl = '../JSON/_storeTasks.JSON';
var donotCallListUrl = '../JSON/_donotCallList.JSON';

var opportunitesListInfo = {};
var scheduledEventsInfo = {};
var storeTasksInfo = {};
var donotCallListInfo = {};

$('#_outreach').live('pagebeforecreate', function (event) {
    $.getJSON(opportunitiesServiceUrl, function (data) {
        opportunitesListInfo = data;
    });

    $.getJSON(scheduledEventsUrl, function (data) {
        scheduledEventsInfo = data;
    });

    $.getJSON(storeTasksUrl, function (data) {
        storeTasksInfo = data;
    });

    $.getJSON(donotCallListUrl, function (data) {
        donotCallListInfo = data;
    });
});

$(document).on('pageshow', '#_outreach', function (event) {

    if ($(window).width() <= 414) {
        $('#tdRightHeader').css('display','none');
    }
    else {
        $('#tdRightHeader').css('display', 'block');
        $('#tdRightHeader').css('display', '');
    }

    jQuery('#txtContactDate').datepicker({
        minDate: new Date(2010, 0, 1),
        dateFormat: 'mm/dd/yy',
        constrainInput: true,
        //beforeShowDay: function (date) {
        //    return displayBlockDays(date, 0);
        //}
    });

    window.setTimeout(function () {
        var pageObj = new outreachViewModel();
        ko.applyBindings(pageObj);
        //
        $('.thermometer-noconfig').thermometer({
            speed: 'slow'
        });
        window.setTimeout(function () {
            $('#ulStoreTasks').listview('refresh');
        }, 200);
    }, 200);
});

var opportunityViewModel = function (opportunity) {
    var self = this;
    ko.mapping.fromJS(opportunity, [], self);
    self.displayName = ko.computed(function () {
        return self.name() + ' - ' + self.phone();
    });
    self.imzColor = ko.observable((self.imz().indexOf("Contract Initiated") > -1 || self.imz().indexOf("Contract Sent") > -1) ? 'green' : '');
    self.soColor = ko.observable((self.so().indexOf("Event Scheduled") > -1) ? 'green' : ((self.so().indexOf("No Outreach") > -1) ? 'red' : ''));
};

var scheduledEventViewModel = function (event) {
    var self = this;
    ko.mapping.fromJS(event, [], self);
    self.statusColor = ko.observable((self.status().indexOf("Canceled") > -1 || self.status().indexOf("Confirm Event") > -1) ? 'red' : 'green');
};

var elementViewModel = function (element) {
    var self = this;
    ko.mapping.fromJS(element, [], self);
};

var outreachViewModel = function () {
    var self = this;
    var store_tasks_count = 0;

    self.opportunities = ko.observableArray();
    self.upcomingEvents = ko.observableArray();
    self.pastEvents = ko.observableArray();
    self.storeTasks = ko.observableArray();
    self.donotCallList = ko.observableArray();
    self.selectedEvent = ko.observable();
    self.selectedOpportunity = ko.observable({});
    self.selectedPreviousOpportunity = ko.observable({});    

    $.each(opportunitesListInfo, function (key, val) {
        self.opportunities.push(new opportunityViewModel(val));
    });

    $.each(scheduledEventsInfo, function (key, val) {
        if (key.toLowerCase() == 'upcomingevents') {
            $.each(val, function (key1, val1) {
                self.upcomingEvents.push(new scheduledEventViewModel(val1));
            });
        }
        else if (key.toLowerCase() == 'pastevents') {
            $.each(val, function (key1, val1) {
                self.pastEvents.push(new scheduledEventViewModel(val1));
            });
        }
    });

    $.each(donotCallListInfo, function (key, val) {
        self.donotCallList.push(new elementViewModel(val));
    });

    $.each(storeTasksInfo, function (key, val) {
        self.storeTasks.push(new elementViewModel(val));
        store_tasks_count += parseInt(val.count);
    });

    self.storeTasksCount = ko.observable(store_tasks_count);
    self.opportunityClick = function (data, event) {
        var ctrl_id = data.name().replace(/\*/g,'_').replace(/-/g,'_').replace(/ /g,'') + ko.contextFor(event.target).$index();
        var temp_data = ko.mapping.toJS(data, mapping);
        if ((data.imz().indexOf('No Client Interest') > -1 || data.imz().indexOf('Business Closed') > -1))
            return false;
        self.selectedPreviousOpportunity(temp_data);
        self.selectedOpportunity(data);
        $('#opportunityMenu').popup('open', { positionTo: '#' + ctrl_id });
    };

    self.cancelContactClick = function () {
        self.selectedOpportunity().name(self.selectedPreviousOpportunity().name);
        self.selectedOpportunity().lastContact(self.selectedPreviousOpportunity().lastContact);
        $('#contactLog').popup('close');
    };

    self.openContactLogPopup = function (data, event) {
        // var ctrl = event.target || event.currentTarget;
        var ele = event.target || event.targetElement;
        var data_context = ko.dataFor(ele);
        // self.selectedEmailRecipientFor(parent_context);
        $('#opportunityMenu').popup('close');
        window.setTimeout(function () {
            $('#contactLog').popup('open');
        }, 300);
    };

    self.openEventMenuPopup = function (data, e) {
        var ctrl = e.target || e.targetElement;
        //window.setTimeout(function () {
        var ctrl_id = data.clinicName().replace(/\*/g, '_').replace(/-/g, '_').replace(/ /g, '');
        self.selectedEvent = data;//ko.mapping.toJS(data, mapping)
        $('#eventMenu').popup('open', { positionTo: '#' + ctrl_id });

        //}, 300);
    };

    self.eventDetailsClick = function () {
        $('#eventMenu').popup('close');
        window.setTimeout(function () {
            window.location.href = '../../clinicDetails/clinicDetails.htm';
        }, 300);
    };

    self.dashboardClick = function () {
        window.setTimeout(function () {
            window.location.href = '../../dashboard/dashboard.htm';
        }, 300);
    };
}