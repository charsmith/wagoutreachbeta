// JavaScript Document
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBarColors);

function drawBarColors() {
      var data = google.visualization.arrayToDataTable([
        ['Season', 'Local', 'Charity', 'Corporate', 'Vote & Vax'],
        ['IMZ 2016-17', 26467, 6735, 2336, 36],
        ['IMZ 2015-16', 22315, 5435, 2526, 0],
        ['IMZ 2014-15', 15797, 3268, 1740, 0]
        
      ]);

      var options = {
        title: 'Scheduled & Completed Clinics Year Over Year',
        chartArea: {width: '50%'},
        colors: ['#e31837', '#7ac143', '#2b7bb7', '#999999'],
        hAxis: {
          title: 'Clinics',
          minValue: 0
        },
        vAxis: {
          title: 'Immunization Season'
        }
      };
      var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }